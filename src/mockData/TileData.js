const tileData = [
  {
    id:1,
    name: "Legendary 1",
    image: "/images/legend.jpg",
    locked: false,
    prizes: [
      { id: 1, name: "prize 1", image: "/images/prizes/prize-1.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 2, name: "prize 2", image: "/images/prizes/prize-2.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 3, name: "prize 3", image: "/images/prizes/prize-3.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 4, name: "prize 4", image: "/images/prizes/prize-4.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 5, name: "prize 5", image: "/images/prizes/prize-5.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
    ]
  },
  {
    id:2,
    name: "Legendary 2",
    image: "/images/legend-2.jpg",
    locked: false,
    prizes: [
      { id: 1, name: "prize 1", image: "/images/prizes/prize-1.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 2, name: "prize 2", image: "/images/prizes/prize-2.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 3, name: "prize 3", image: "/images/prizes/prize-3.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 4, name: "prize 4", image: "/images/prizes/prize-4.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 5, name: "prize 5", image: "/images/prizes/prize-5.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 6, name: "prize 6", image: "/images/prizes/prize-6.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 7, name: "prize 7", image: "/images/prizes/prize-7.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 8, name: "prize 8", image: "/images/prizes/prize-8.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 9, name: "prize 9", image: "/images/prizes/prize-9.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 10, name: "prize 10", image: "/images/prizes/prize-10.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 11, name: "prize 11", image: "/images/prizes/prize-1.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 12, name: "prize 12", image: "/images/prizes/prize-2.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 13, name: "prize 13", image: "/images/prizes/prize-3.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 14, name: "prize 14", image: "/images/prizes/prize-4.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 15, name: "prize 15", image: "/images/prizes/prize-5.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 16, name: "prize 16", image: "/images/prizes/prize-6.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 17, name: "prize 17", image: "/images/prizes/prize-7.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 18, name: "prize 18", image: "/images/prizes/prize-8.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 19, name: "prize 19", image: "/images/prizes/prize-9.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 20, name: "prize 20", image: "/images/prizes/prize-10.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 21, name: "prize 21", image: "/images/prizes/prize-1.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 22, name: "prize 22", image: "/images/prizes/prize-2.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 23, name: "prize 23", image: "/images/prizes/prize-3.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 24, name: "prize 24", image: "/images/prizes/prize-4.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 25, name: "prize 25", image: "/images/prizes/prize-5.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 26, name: "prize 26", image: "/images/prizes/prize-6.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 27, name: "prize 27", image: "/images/prizes/prize-7.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 28, name: "prize 28", image: "/images/prizes/prize-8.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 29, name: "prize 29", image: "/images/prizes/prize-9.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 30, name: "prize 30", image: "/images/prizes/prize-10.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
    ]
  },
  { id:3,
    name: "Legendary 3",
    image: "/images/legend-3.jpg",
    locked: false,
    prizes: [
      { id: 1, name: "prize 1", image: "/images/prizes/prize-1.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 2, name: "prize 2", image: "/images/prizes/prize-2.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 3, name: "prize 3", image: "/images/prizes/prize-3.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 4, name: "prize 4", image: "/images/prizes/prize-4.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 5, name: "prize 5", image: "/images/prizes/prize-5.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 6, name: "prize 6", image: "/images/prizes/prize-6.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 7, name: "prize 7", image: "/images/prizes/prize-7.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 8, name: "prize 8", image: "/images/prizes/prize-8.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 9, name: "prize 9", image: "/images/prizes/prize-9.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 10, name: "prize 10", image: "/images/prizes/prize-10.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 11, name: "prize 11", image: "/images/prizes/prize-1.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 12, name: "prize 12", image: "/images/prizes/prize-2.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 13, name: "prize 13", image: "/images/prizes/prize-3.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 14, name: "prize 14", image: "/images/prizes/prize-4.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 15, name: "prize 15", image: "/images/prizes/prize-5.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 16, name: "prize 16", image: "/images/prizes/prize-6.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 17, name: "prize 17", image: "/images/prizes/prize-7.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 18, name: "prize 18", image: "/images/prizes/prize-8.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 19, name: "prize 19", image: "/images/prizes/prize-9.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 20, name: "prize 20", image: "/images/prizes/prize-10.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 21, name: "prize 21", image: "/images/prizes/prize-1.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 22, name: "prize 22", image: "/images/prizes/prize-2.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 23, name: "prize 23", image: "/images/prizes/prize-3.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 24, name: "prize 24", image: "/images/prizes/prize-4.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 25, name: "prize 25", image: "/images/prizes/prize-5.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 26, name: "prize 26", image: "/images/prizes/prize-6.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 27, name: "prize 27", image: "/images/prizes/prize-7.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 28, name: "prize 28", image: "/images/prizes/prize-8.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 29, name: "prize 29", image: "/images/prizes/prize-9.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 30, name: "prize 30", image: "/images/prizes/prize-10.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 31, name: "prize 31", image: "/images/prizes/prize-1.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 32, name: "prize 32", image: "/images/prizes/prize-2.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 33, name: "prize 33", image: "/images/prizes/prize-3.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 34, name: "prize 34", image: "/images/prizes/prize-4.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 35, name: "prize 35", image: "/images/prizes/prize-5.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 36, name: "prize 36", image: "/images/prizes/prize-6.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 37, name: "prize 37", image: "/images/prizes/prize-7.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 38, name: "prize 38", image: "/images/prizes/prize-8.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 39, name: "prize 39", image: "/images/prizes/prize-9.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 40, name: "prize 40", image: "/images/prizes/prize-10.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 41, name: "prize 41", image: "/images/prizes/prize-1.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 42, name: "prize 42", image: "/images/prizes/prize-2.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 43, name: "prize 43", image: "/images/prizes/prize-3.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 44, name: "prize 44", image: "/images/prizes/prize-4.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 45, name: "prize 45", image: "/images/prizes/prize-5.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 46, name: "prize 46", image: "/images/prizes/prize-6.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 47, name: "prize 47", image: "/images/prizes/prize-7.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 48, name: "prize 48", image: "/images/prizes/prize-8.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 49, name: "prize 49", image: "/images/prizes/prize-9.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 50, name: "prize 50", image: "/images/prizes/prize-10.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 51, name: "prize 51", image: "/images/prizes/prize-1.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 52, name: "prize 52", image: "/images/prizes/prize-2.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 53, name: "prize 53", image: "/images/prizes/prize-3.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 54, name: "prize 54", image: "/images/prizes/prize-4.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 55, name: "prize 55", image: "/images/prizes/prize-5.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 56, name: "prize 56", image: "/images/prizes/prize-6.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 57, name: "prize 57", image: "/images/prizes/prize-7.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 58, name: "prize 58", image: "/images/prizes/prize-8.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 59, name: "prize 59", image: "/images/prizes/prize-9.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },

    ]
  },
  { id:4,
    name: "Mythical",
    image: "/images/mythic.jpg",
    locked: true,
    prizes: [
      { id: 1, name: "prize 1", image: "/images/prizes/prize-1.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 2, name: "prize 2", image: "/images/prizes/prize-2.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 3, name: "prize 3", image: "/images/prizes/prize-3.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 4, name: "prize 4", image: "/images/prizes/prize-4.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 5, name: "prize 5", image: "/images/prizes/prize-5.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 6, name: "prize 6", image: "/images/prizes/prize-6.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 7, name: "prize 7", image: "/images/prizes/prize-7.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 8, name: "prize 8", image: "/images/prizes/prize-8.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 9, name: "prize 9", image: "/images/prizes/prize-9.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 10, name: "prize 10", image: "/images/prizes/prize-10.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 11, name: "prize 11", image: "/images/prizes/prize-1.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 12, name: "prize 12", image: "/images/prizes/prize-2.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 13, name: "prize 13", image: "/images/prizes/prize-3.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 14, name: "prize 14", image: "/images/prizes/prize-4.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 15, name: "prize 15", image: "/images/prizes/prize-5.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 16, name: "prize 16", image: "/images/prizes/prize-6.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 17, name: "prize 17", image: "/images/prizes/prize-7.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 18, name: "prize 18", image: "/images/prizes/prize-8.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 19, name: "prize 19", image: "/images/prizes/prize-9.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 20, name: "prize 20", image: "/images/prizes/prize-10.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 21, name: "prize 21", image: "/images/prizes/prize-1.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 22, name: "prize 22", image: "/images/prizes/prize-2.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 23, name: "prize 23", image: "/images/prizes/prize-3.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 24, name: "prize 24", image: "/images/prizes/prize-4.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 25, name: "prize 25", image: "/images/prizes/prize-5.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 26, name: "prize 26", image: "/images/prizes/prize-6.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 27, name: "prize 27", image: "/images/prizes/prize-7.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 28, name: "prize 28", image: "/images/prizes/prize-8.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 29, name: "prize 29", image: "/images/prizes/prize-9.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 30, name: "prize 30", image: "/images/prizes/prize-10.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
    ]
  },
  { id:5,
    name: "Discount",
    image: "/images/discount.jpg",
    locked: false,
    discount: true,
    prizes: [
      { id: 1, name: "prize 1", image: "/images/prizes/prize-1.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 2, name: "prize 2", image: "/images/prizes/prize-2.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 3, name: "prize 3", image: "/images/prizes/prize-3.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 4, name: "prize 4", image: "/images/prizes/prize-4.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 5, name: "prize 5", image: "/images/prizes/prize-5.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 6, name: "prize 6", image: "/images/prizes/prize-6.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 7, name: "prize 7", image: "/images/prizes/prize-7.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 8, name: "prize 8", image: "/images/prizes/prize-8.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 9, name: "prize 9", image: "/images/prizes/prize-9.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 10, name: "prize 10", image: "/images/prizes/prize-10.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 11, name: "prize 11", image: "/images/prizes/prize-1.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 12, name: "prize 12", image: "/images/prizes/prize-2.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 13, name: "prize 13", image: "/images/prizes/prize-3.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 14, name: "prize 14", image: "/images/prizes/prize-4.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 15, name: "prize 15", image: "/images/prizes/prize-5.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 16, name: "prize 16", image: "/images/prizes/prize-6.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 17, name: "prize 17", image: "/images/prizes/prize-7.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 18, name: "prize 18", image: "/images/prizes/prize-8.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 19, name: "prize 19", image: "/images/prizes/prize-9.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
      { id: 20, name: "prize 20", image: "/images/prizes/prize-10.png", locked: false, quantity: 10, description: "This is not the prize you're looking for. *moves hand in sweeping motion*" },
    ]
  }
];

export default tileData;
