import React, { Component } from 'react';
import './Tile.css';

class Tile extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let saleBadge = null;

    if (this.props.tile.discount) {
      saleBadge = <img className="sale-badge" src="/images/sale.png" />
    }
    return (
      <div className={this.props.type} data-tile={JSON.stringify(this.props.tile)}
          onClick={this.props.tileHandler} style={{ "backgroundImage": `url(${this.props.tile.image})` }}>
        <div className={this.props.tile.locked ? "tile-overlay-locked" : "tile-overlay"}></div>
        {saleBadge}
        <h3 className="tile-name">{this.props.tile.name}</h3>
        { this.props.quantity ? <p className="prize-quantity">x{this.props.tile.quantity}</p> : null }
        <LockedSection locked={this.props.tile.locked} />
      </div>
    );
  }
}

function LockedSection(props) {
  let lockedSection = (
    <div className="locked h-center-absolute">
      <img src="/images/lock.png" alt="locked" />
      <p>Info</p>
    </div>
  );

  if (!props.locked)
    lockedSection = null;

  return lockedSection;
}

export default Tile;
