import React, { Component } from 'react';
import './HomeNavigation.css';
import RewardView from '../RewardView/RewardView';

class HomeNavigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTab: "rewards"
    }
  }

  handleTabClick = (e) => {
    this.setState({currentTab: e.currentTarget.id});
  }

  render() {
    return (
      <div className="HomeNavigation">
        <ul className="navigationContainer moduleSection">
          <li id="battle" className={"navigationTabs" + (this.state.currentTab == "battle" ? " navigationTabs--selected": "")} onClick={this.handleTabClick}><p>Battle</p></li>
          <li id="prizes" className={"navigationTabs" + (this.state.currentTab == "prizes" ? " navigationTabs--selected": "")} onClick={this.handleTabClick}><p>Prizes & Ranks</p></li>
          <li id="rewards" className={"navigationTabs" + (this.state.currentTab == "rewards" ? " navigationTabs--selected": "")} onClick={this.handleTabClick}><p>Rewards</p></li>
          <li id="armory" className={"navigationTabs" + (this.state.currentTab == "armory" ? " navigationTabs--selected": "")} onClick={this.handleTabClick}><p>Armory</p></li>
        </ul>

        <HomeNavigationDisplay tab={this.state.currentTab}/>
      </div>
    );
  }
}

function HomeNavigationDisplay(props) {
  let display = <h3> HomeNavigationDisplay </h3>;

  switch(props.tab) {
    case "rewards":
      display = <RewardView />;
      break;

    default:
      display = <h3 className="error-message"> Page currently not functional. Please Navigate to "Rewards" tab</h3>
  }

  return (
    <div className="HomeNavigationDisplay moduleSection">
    {display}
    </div>
  );
}

export default HomeNavigation;
