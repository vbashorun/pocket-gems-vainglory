import React, { Component } from 'react';
import './App.css';
import HomeNavigation from '../HomeNavigation/HomeNavigation.js';

class App extends Component {
  render() {
    return (
      <div className="App">
        <HomeNavigation />
      </div>
    );
  }
}

export default App;
