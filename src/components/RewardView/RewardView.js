import React, { Component } from 'react';
import './RewardView.css';
import Tile from '../Tile/Tile';
import PrizeView from '../PrizeView/PrizeView';
import tileData from '../../mockData/TileData';

class RewardView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rewardTiles: [],
      prizeTiles: [],
      showLockOverlay: false,
      showPrizeView: false,
    };

    this.tileData = tileData;
  }

  handleTileClick = (e) => {
    let selectedTile = JSON.parse(e.currentTarget.dataset.tile);

    if (selectedTile) {

      if (selectedTile.locked) {
        this.lockToggle();
      }

      else {
        let prizeTiles = [];

        if (selectedTile.prizes && selectedTile.prizes.length > 0) {
          this.setState({selectedTile: selectedTile, showPrizeView: true });
        }
      }
    }
  }

  prizeToggle = () => {
    this.setState((prevState, props) => ({showPrizeView: !prevState.showPrizeView}));
  }

  lockToggle = () => {
    this.setState((prevState, props) => ({showLockOverlay: !prevState.showLockOverlay}));
  }

  componentWillMount = () => {
    let rewardTiles = [];

    for(let tile of this.tileData) {
      rewardTiles.push(<Tile key={tile.id} tile={tile}
        tileHandler={this.handleTileClick} type="Tile" />);
    };

    let listWidth = rewardTiles.length * 35;

    this.setState({rewardTiles: rewardTiles, listWidth: listWidth });
  }

  render() {

    let prizeView = null, lockOverlay = null;

    if (this.state.showPrizeView)
      prizeView = <PrizeView selectedTile={this.state.selectedTile} overlayToggle={this.prizeToggle} />;

    if (this.state.showLockOverlay)
      lockOverlay = <LockOverlay overlayToggle={this.lockToggle} />;

    return (
      <div className="Reward-view">
        {lockOverlay}
        {prizeView}
        <h3 className="section-title">Fall Season</h3>
        <div className="h-menu-container">
          <div className="h-menu" style={{ width: this.state.listWidth + "vw" }}>
          {this.state.rewardTiles}
          </div>
        </div>
      </div>
    );
  }
}

function LockOverlay(props) {
  return (
    <div id="lockOverlay">
      <div className="overlay-info absolute-center">
        <img className="overlay-close" src="/images/x.png" onClick={props.overlayToggle} />
        <p>
          Item Locked
          <br/>
          Complete the 3 Legendary branches to unlock
        </p>
      </div>
    </div>
  );
}

export default RewardView;
