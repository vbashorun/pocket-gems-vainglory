import React, { Component } from 'react';
import './PrizeView.css';
import Tile from '../Tile/Tile';

class PrizeView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentPage: 1,
      totalPages: 1,
      pageLength: 28,
      showPrizeOverlay: false,
      prizeTileDisplay: [],
      selectedTile: {},
      tileContainerOpacity: 1
    };
  }

  handleTileClick = (e) => {
    let justSelected = JSON.parse(e.currentTarget.dataset.tile);
    this.setState({ selectedTile: justSelected }, () => { this.overlayToggle(); });
  }

  processPage = () => {
    this.setState({totalPages: Math.ceil(this.prizeTiles.length / this.state.pageLength)});

    let end = this.state.pageLength * this.state.currentPage,
    start = end - this.state.pageLength;

    let pagination = this.prizeTiles.slice(start, end);

    this.setState({prizeTileDisplay: pagination});
  }

  pageLeft = () => {
    if (this.state.currentPage > 1) {
      var tileContainer = document.getElementsByClassName('prize-tile-container')[0];
      tileContainer.style.opacity = 0;

      this.setState((prevState, props) => ({ currentPage: --prevState.currentPage }), () => {
        setTimeout(() => {
          this.processPage();
          tileContainer.style.opacity = 1;
        }, 1000);
      });
    }
  }

  pageRight = () => {
    if (this.state.currentPage < this.state.totalPages) {
      var tileContainer = document.getElementsByClassName('prize-tile-container')[0];
      tileContainer.style.opacity = 0;

      this.setState((prevState, props) => ({ currentPage: ++prevState.currentPage }), () => {
        setTimeout(() => {
          this.processPage();
          tileContainer.style.opacity = 1;
        }, 1000);
      });
    }
  }

  overlayToggle = () => {
    this.setState((prevState, props) => ({showPrizeOverlay: !prevState.showPrizeOverlay}));
  }

  componentWillMount = () => {
    let selectedTile = this.props.selectedTile;

    this.prizeTiles = [];

    if (selectedTile.prizes && selectedTile.prizes.length > 0) {
      for(let tile of selectedTile.prizes) {
        this.prizeTiles.push(<Tile key={tile.id} tileId={tile.id} name={tile.name}
          image={tile.image} locked={tile.locked} tileHandler={this.handleTileClick}
          quantity={tile.quantity} description={tile.description} tile={tile} type="Prize-Tile" />);
      }
    }

    this.processPage();
  }

  render() {
    let pageLeft = null, pageRight = null, prizeOverlay = null;

    if (this.state.totalPages > 1 && this.state.currentPage != 1)
      pageLeft = <img src="/images/page_left.png" className="page-control page-left" onClick={this.pageLeft} />;

    if (this.state.totalPages > 1 && this.state.currentPage != this.state.totalPages)
      pageRight = <img src="/images/page_right.png" className="page-control page-right" onClick={this.pageRight} />;

    if (this.state.showPrizeOverlay)
      prizeOverlay = <PrizeOverlay overlayToggle={this.overlayToggle} selectedTile={this.state.selectedTile} />;


    return (
      <div className="Prize-View">
        {pageLeft}
        {pageRight}
        {prizeOverlay}
        <img className="overlay-close" src="/images/x.png" onClick={this.props.overlayToggle} />
        <h3 className="section-title">{this.props.selectedTile.name} Branch</h3>
        <h6 className="section-sub-title">Page {this.state.currentPage} of {this.state.totalPages}</h6>
        <div className="prize-tile-container opacity-fade">
          {this.state.prizeTileDisplay}
        </div>
      </div>
    );
  }
}

function PrizeOverlay(props) {
  return (
    <div id="prizeOverlay">
      <div className="overlay-info absolute-center">
        <img className="overlay-close" src="/images/x.png" onClick={props.overlayToggle} />
        <p>{props.selectedTile.description}</p>
      </div>
    </div>
  );
}

export default PrizeView;
